import Holder from 'holderjs';
import React, { Component } from 'react';
import { Grid, Nav, Navbar, NavItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { HashRouter, NavLink, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import CourceDetails from './routes/CourceDetails';
import CourceElement from './routes/CourceElement';
import Main from './routes/Main';

const store = createStore(s => s);

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <HashRouter>
          <div>
            <Navbar inverse collapseOnSelect>
              <Navbar.Header>
                <Navbar.Brand>
                  <NavLink to="/">Speaking English</NavLink>
                </Navbar.Brand>
                <Navbar.Toggle />
              </Navbar.Header>
              <Navbar.Collapse>
                <Nav pullRight>
                  <LinkContainer to="/login">
                    <NavItem eventKey={1}>
                      Log In
                    </NavItem>
                  </LinkContainer>
                </Nav>
              </Navbar.Collapse>
            </Navbar>
            <Grid>
              <Route exact path="/" component={Main}/>
              <Route path="/cources/:courceId/info" component={CourceDetails}/>
              <Route path="/cources/:courceId/ch/:chId" component={CourceElement}/>
            </Grid>
          </div>
        </HashRouter>
      </Provider>
    );
  }
}

export default App;
