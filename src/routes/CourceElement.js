import React, { Component } from 'react';
import { Button, ButtonGroup, Col, Image, Jumbotron, Row } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

class CourceElement extends Component {
  render() {
    return (
      <Row>
        <Col xs={12}>
          <Jumbotron className="text-center">
            <Image src="holder.js/600x400" rounded/>
            <h3>Word in English</h3>
            <p>translation</p>
            <p>Usage</p>
            <hr/>
            <ButtonGroup justified>
              <LinkContainer to="/cources/1/ch/1">
                <Button><span className="glyphicon glyphicon-fast-backward" aria-hidden="true"></span></Button>
              </LinkContainer>
              <LinkContainer to="/cources/1/ch/1">
                <Button><span className="glyphicon glyphicon-ok" aria-hidden="true"></span></Button>
              </LinkContainer>
              <LinkContainer to="/cources/1/ch/1">
                <Button><span className="glyphicon glyphicon-fast-forward" aria-hidden="true"></span></Button>
              </LinkContainer>
            </ButtonGroup>
          </Jumbotron>
        </Col>
      </Row>
    );
  }
}

export default CourceElement;
