import React, { Component } from 'react';
import { Col, Jumbotron, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';

class CourceDetails extends Component {
  render() {
    return (
      <Row>
        <Col xs={12}>
          <Jumbotron>
            <h1>Информация курса:</h1>
          </Jumbotron>
          <Row>
            <Col xs={6} lg={4}>
              <h2>Greeting.</h2>
              <p>Many ways to say "Hi!"</p>
              <p><Link to="/cources/1/ch/1" className="btn btn-default" role="button">View details »</Link></p>
            </Col>
            <Col xs={6} lg={4}>
              <h2>Small talk.</h2>
              <p>Many ways to start a conversation.</p>
              <p><Link to="/cources/1/ch/2" className="btn btn-default" role="button">View details »</Link></p>
            </Col>
            <Col xs={6} lg={4}>
              <h2>To make it clear.</h2>
              <p>Many ways to get an opinion.</p>
              <p><Link to="/cources/1/ch/3" className="btn btn-default" role="button">View details »</Link></p>
            </Col>
            <Col xs={6} lg={4}>
              <h2>Accommodation.</h2>
              <p>Questions and answers about accommodation. </p>
              <p><Link to="/cources/1/ch/4" className="btn btn-default" role="button">View details »</Link></p>
            </Col>
            <Col xs={6} lg={4}>
              <h2>Family.</h2>
              <p>Questions and answers about family.  </p>
              <p><Link to="/cources/1/ch/5" className="btn btn-default" role="button">View details »</Link></p>
            </Col>
            <Col xs={6} lg={4}>
              <h2>Habits and hobby.</h2>
              <p>Questions and answers about habits and hobby. </p>
              <p><Link to="/cources/1/ch/6" className="btn btn-default" role="button">View details »</Link></p>
            </Col>
            <Col xs={6} lg={4}>
              <h2>Farewell.</h2>
              <p>Many ways to say "Bye".</p>
              <p><Link to="/cources/1/ch/7" className="btn btn-default" role="button">View details »</Link></p>
            </Col>
          </Row>
        </Col>
      </Row>
    );
  }
}

export default CourceDetails;
