import React, { Component } from 'react';
import { Col, Jumbotron, Row } from 'react-bootstrap';
import { Link } from 'react-router-dom';

class Main extends Component {
  render() {
    return (
      <Row>
        <Col xs={12}>
          <Jumbotron>
            <h1>Доступные курсы:</h1>
          </Jumbotron>
          <Row>
            <Col xs={6} lg={4}>
              <h2>Разговорный курс английского языка</h2>
              <p>Этот курс включает в себя 12 блоков.</p>
              <p><Link to="/cources/1/info" className="btn btn-default" role="button">View details »</Link></p>
            </Col>
            <Col xs={6} lg={4}>
              <h2>Testing</h2>
              <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
              <p><Link to="/cources/2/info" className="btn btn-default" role="button">View details »</Link></p>
            </Col>
            <Col xs={6} lg={4}>
              <h2>Java</h2>
              <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
              <p><Link to="/cources/3/info" className="btn btn-default" role="button">View details »</Link></p>
            </Col>
          </Row>
        </Col>
      </Row>
    );
  }
}

export default Main;
